<?php

return array(

    'introTitle' => 'Family Matters!',
    'intro' =>  'HEPPEE is dé mobiele app voor kinderen én hun (gescheiden) ouders die de communicatie en organisatie van het gezinsleven gemakkelijk en leuker maakt!',
    'what' =>   'Wat is Heppee?',
    'signup' => 'Meld je aan!',

    'about' => 'Voor kinderen is een scheiding een heel heftige ervaring. Hun veilige leven is ineens verdwenen en ze kunnen zich geen beeld vormen van de toekomst. Ze komen in een loyaliteitsconflict, zijn verdrietig, bang of boos.<br><br> Plannen en communiceren in een gescheiden situatie is zeer lastig terwijl kinderen nu juist op zoek zijn naar zekerheden!',
    'aboutTitle' => 'Heppee is de communicatie &amp; organisatietool voor gescheiden ouders waarbij het kind centraal staat.',
    'aboutDesc' =>  'De app heeft een kindvriendelijke user interface en maakt drie dingen beter en leuker:',

    'emotionTitle' => 'Emotie',
    'emotionDesc' => 'Omdat kinderen vaak te kort bij hun ouders zijn om te laten merken dat ze niet lekker in hun vel zitten. Via een moodbutton kunnen ze hun gevoel delen.',

    'organisationTitle' => 'Organisatie',
    'organisationDesc' => 'Essentieel onderdeel van een gescheiden gezinssituatie is planning. Voorkom onnodige irritaties met de slimme familie-agenda.',

    'communicationTitle' => 'Communicatie',
    'communicationDesc' => 'Denk hierbij aan het delen van gegevens zoals kledingmaten, medische gegevens, allergieën, etc. Maar ook beeld en video. Denk aan dat eerste tandje...',

    'partners'  =>  'Partners who make us Heppee',

    'get-in-touch' => 'Contact',
    'get-in-touch-desc' => 'We horen graag van u, stuur ons eens een mailtje of pak de telefoon voor snel een praatje.',

    'more-info' => 'Meer informatie',
    'our-team' => 'Ons Team',
    'press' => 'Persberichten',
    'privacy' => 'Privacy en disclaimer',
    'terms' => 'Algemene voorwaarden',

    'community' =>  'Community',
    'blog' => 'Blog',
    'forums' => 'Forums',
    'guestblogging' => 'Guestbloggen',

    'follow' => 'Volg Heppee!',

    'support' => 'Ondersteuning',
    'call' => 'Bel gratis:',

    'appReady' => 'Ontvang een mailtje wanneer onze app te downloaden is!',

    'openForm' => 'Aanmelden als testgezin!',
);
