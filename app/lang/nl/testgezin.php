<?php

return array(

    'intro' => '
Wij vinden het ontzettend fijn dat je wilt deelnemen aan onze testgroep. Jouw ervaringen en mening zijn voor ons erg van belang.<br>

Ons team begrijpt zelf ook erg goed dat een scheiding en co-ouderschap een ingrijpende situatie is.

We willen je alvast bedanken voor je medewerking.',

    'outro' => 'Heb je vragen of je wilt iets aan ons kwijt? Aarzel niet en neem contact op via <a href="mailto:sabrina@heppee.com">sabrina@heppee.com</a>.'

);
