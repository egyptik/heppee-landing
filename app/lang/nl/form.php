<?php

return array(

    'your-details' => 'Uw gegevens',

    'name' => 'Naam',
    'email' => 'E-mail',
    'phone' => 'Telefoon nr.',

    'roles' => [
        'role' => 'Rol:',
        'mother' => 'Moeder',
        'father' => 'Vader',
        'stepmother' => 'Stiefmoeder',
        'stepfather' => 'Stiefvader'
    ],

    'birthyear' => 'Geboortejaar:',

    'children' => 'Kinderen',

    'genders' => [
        'gender' => 'Geslacht:',
        'boy'   => 'Jongen',
        'girl' => 'Meisje'
    ],

    'add-child' => 'Nog een kind toevoegen',
    'delete-child' => 'Een kind verwijderen',

    'submit' => 'Aanmelden!',

    'signup-success' => 'Bedankt voor het aanmelden! U ontvangt een bevestigingsmail op het opgegeven e-mailadres.',

);