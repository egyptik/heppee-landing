<?php

return array(

    'intro' => 'We are happy with your participation in our testgroup. Your experience, lessons and opinion are very important to us.<br>

Our team understands very well that divorce and co-parenting is a drastic situation.

We want to thank you for your cooperation!',

    'outro' => 'Do you have any questions or comments? Don\'t hesitate to contact us via <a href="mailto:sabrina@heppee.com">sabrina@heppee.com</a>.'

);
