<?php

return array(

    'your-details' => 'Your details',

    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone',

    'roles' => [
        'role' => 'Role:',
        'mother' => 'Mother',
        'father' => 'Father',
        'stepmother' => 'Stepmother',
        'stepfather' => 'Stepfather'
    ],

    'birthyear' => 'Birthyear:',

    'children' => 'Children',

    'genders' => [
        'gender' => 'Gender:',
        'boy'   => 'Boy',
        'girl' => 'Girl'
    ],

    'add-child' => 'Add another child',
    'delete-child' => 'Delete child',

    'submit' => 'Signup!',

    'signup-success' => 'Thanks! A confirmation email has been sent to your email address.',
);