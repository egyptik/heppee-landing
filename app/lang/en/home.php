<?php
return array(

    'introTitle' => 'Family Matters!',
    'intro' =>  'HEPPEE is the mobile app for children and their divorced parents to ease the pain of communication and organisation of family life!',
    'what' =>   'What is Heppee?',
    'signup' => 'Signup!',

    'about' => 'For children a divorce is a severe event. Sometimes it can even be devastating. Suddenly the safe and stable family environment is disrupted and the future insecure and doubtful. Children in these situations often encounter a loyalty conflict, they may be sad and distressed and sometimes also feel anger and fear.<br><br> Planning and communication in a divorced family setting is often challenging, while the children are now even more looking for safety and security.',
    'aboutTitle' => 'Heppee is the communication &amp; organisation tool for divorced parents in which the child takes the center position.',
    'aboutDesc' =>  'The app has a child-friendly interface and targets three important aspects:',

    'emotionTitle' => 'Emotion',
    'emotionDesc' => 'Children often find it difficult to find the right moment to ventilate their real feelings, Heppee provides a mood button to give children an easy way to share their feelings!',

    'organisationTitle' => 'Organisation',
    'organisationDesc' => 'Planning is an crucial element in a divorced family setting. Heppee helps you to avoid unneeded irritations.',

    'communicationTitle' => 'Communication',
    'communicationDesc' => 'Wouldn’t it be great if you could easily share stuff like clothing sizes, medical details and allergies, do’s and don’ts, And off course also pictures and video’s!',

    'partners'  =>  'Partners who make us Heppee',

    'get-in-touch' => 'Get in touch',
    'get-in-touch-desc' => 'We\'d love to hear from you, so why not send us an email or pick up the phone for a quick chat.',

    'more-info' => 'More information',
    'our-team' => 'Our Team',
    'press' => 'Pressreleases',
    'privacy' => 'Privacy and disclaimer',
    'terms' => 'Terms of Service',

    'community' =>  'Community',
    'blog' => 'Blog',
    'forums' => 'Forums',
    'guestblogging' => 'Guestblogging',

    'follow' => 'Follow Heppee!',

    'support' => 'Support',
    'call' => 'Call:',

    'appReady' => 'Be notified when the app is available to download!',

    'openForm' => 'Signup as beta tester!',
);