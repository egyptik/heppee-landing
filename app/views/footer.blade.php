<div class="footer" style="background: #008ed6 !important; padding-top: 50px; padding-bottom:0px; color:#fff; border-top: 6px solid #ddd;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <ul class="unstyled" style="text-align: left;">
                        <li style="margin-bottom: 5px;"><strong>{{ Lang::get('home.more-info') }}</strong></li>
                        <li><a href="http://www.heppee.com/team">{{ Lang::get('home.our-team') }}</a></li>
                        <li><a href="http://heppee.com/blog/pers-media/">{{ Lang::get('home.press') }}</a></li>
                        <li><a href="http://forum.heppee.com/privacy">{{ Lang::get('home.privacy') }}</a></li>
                    </ul>
                </div>
                <div class="col-md-3" style="text-align: left;">
                    <ul class="unstyled">
                        <li style="margin-bottom: 5px;"><strong>{{ Lang::get('home.community') }}</strong></li>
                        <li><a href="http://heppee.com/blog">{{ Lang::get('home.blog') }}</a></li>
                        <li><a href="http://forum.heppee.com">{{ Lang::get('home.forums') }}</a></li>
                        <li><a href="http://heppee.com/blog/gastbloggen">{{ Lang::get('home.guestblogging') }}</a></li>
                        <li><a href="http://forum.heppee.com/signup">{{ Lang::get('home.signup') }}</a></li>
                    </ul>
                </div>
                <div class="col-md-3" style="text-align: left;">
                    <ul class="unstyled">
                        <li style="margin-bottom: 5px;"><strong>{{ Lang::get('home.follow') }}</strong></li>
                        <li><a href="https://www.facebook.com/heppee" target="_blank">Facebook</a></li>
                        <li><a href="http://www.twitter.com/heppeeapp" target="_blank">Twitter</a></li>
                        <!-- <li><a href="#">Google Plus</a></li> -->
                    </ul>
                </div>
                <div class="col-md-3" style="text-align: left;">
                    <ul class="unstyled">
                        <li style="margin-bottom: 5px;"><strong>Contact</strong></li>
                        <li><a href="mailto:thierry@heppee.com" target="_blank">Thierry@heppee.com</a></li>
                        <li><a href="tel:+31641219024" target="_blank">+316 41 21 90 24</a></li>
                        <li>Overtoom 197-1</li>
                        <li>1054 HT Amsterdam</li>
                    </ul>

                </div>
            </div>
        </div>
        <p class="copyright" style="margin-top: 50px;color:#fff;">
            &copy; {{ date('Y') }} Heppee, All Rights Reserved.
        </p>
    </div>