<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Heppee.com</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

    {{ HTML::style('css/beta.css') }}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-55465707-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body>
<div id="background">

<div class="container hidden-sm" style="padding-top:12%;">

    <div class="col-md-6 col-lg-6 col-sm-6 hidden-xs">
        <a data-toggle="modal" data-target="#videoModal" style="cursor:pointer;">
        <img src="img/beta/iphones.png" alt="" class="phones img-responsive"/>
        <div style="position: absolute; background: url('http://dev.csda.net/wp-content/uploads/2013/11/play.png') center center no-repeat; width:100px; height:100px; top:200px;left:200px;"></div>
        </a>
    </div>

    <div class="col-md-5">
        <img src="img/logo.png" alt="Heppee" class="logo">
        <div style="margin-top: 40px; margin-right: 25px;" class="pull-right">
            <a href="http://heppee.com/blog/pers-media/" style="color:#fff;">Pers</a> &nbsp;|&nbsp; <a href="http://heppee.com/blog/faq/" style="color:#fff;">FAQ</a>
        </div>

        <p class="lead">Let's make it a Childhood our kids will always remember</p>
        <p style="color:#818181;text-shadow: none;">De Co-ouderschap app voor kinderen en hun ouders. Meld je aan voor onze besloten testversie!</p>

        <form id="subscribe" class="form" action="<?=$_SERVER['PHP_SELF']; ?>" method="post" style="margin-top: 25px; background:rgba(0,0,0,0.5); padding: 15px; padding-top:20px; border-radius:5px;">
            <span id="response"><? require_once('mailchimp2/inc/store-address.php'); if(isset($_GET['submit'])){ echo storeAddress(); } ?></span>
            <div class="form-group">
                <label>Welk type smartphone heeft u?</label>
                <br>
                <input type="radio" name="phone" value="iphone" checked> &nbsp;iPhone
                &nbsp;&nbsp;&nbsp;
                <input type="radio" name="phone" value="android"> &nbsp;Android
            </div><br>
            <div class="form-group form-inline">
                <div class="input-group">
                    <input size="25" type="email" class="form-control input-box required input-lg" id="email" name="email" placeholder="emailadres" style="border: 0px;">
                    <span class="input-group-btn">
                      <input type="submit" class="btn btn-primary standard-button2 ladda-button" style="padding-top: 12px; padding-bottom: 12px; padding-left: 25px; padding-right: 25px; background: #06ABFF; border:1px solid #06ABFF;" value="SUBSCRIBE">
                    </span>
                </div>
            </div>
        </form>

        <div class="row" style="margin-top: 25px; margin-bottom: 25px;">
            <div class="col-md-12">
                <center>
                    <ul class="share-buttons">
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fheppee.com&t=<?php echo urlencode("HEPPEEapp dé co-ouderschap app voor kinderen en hun ouders. Meld je aan voor onze besloten testversie via heppee.com"); ?>" target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=<?php echo urlencode("HEPPEEapp dé co-ouderschap app voor kinderen en hun ouders. Meld je aan voor onze besloten testversie via heppee.com"); ?>'); return false;"><img src="images/flat_web_icon_set/black/Facebook.png"></a></li>
                        <li><a href="https://twitter.com/intent/tweet?source=http%3A%2F%2Fheppee.com&text=<?php echo urlencode("@HEPPEEapp dé co-ouderschap app voor kinderen en hun ouders. Meld je aan voor onze besloten testversie via heppee.com"); ?>via=heppeeapp" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=<?php echo urlencode("@HEPPEEapp dé co-ouderschap app voor kinderen en hun ouders. Meld je aan voor onze besloten testversie via heppee.com"); ?>:%20'); return false;"><img src="images/flat_web_icon_set/black/Twitter.png"></a></li>
                        <li><a href="https://plus.google.com/share?url=http%3A%2F%2Fheppee.com" target="_blank" title="Share on Google+" onclick="window.open('https://plus.google.com/share?url=' + encodeURIComponent(document.URL)); return false;"><img src="images/flat_web_icon_set/black/Google+.png"></a></li>
                        <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fheppee.com&title=&summary=&source=http%3A%2F%2Fheppee.com" target="_blank" title="Share on LinkedIn" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(document.URL) + '&title=<?php echo urlencode("HEPPEEapp dé co-ouderschap app voor kinderen en hun ouders. Meld je aan voor onze besloten testversie via heppee.com"); ?>'); return false;"><img src="images/flat_web_icon_set/black/LinkedIn.png"></a></li>
                        <li><a href="mailto:?subject=&body=:%20http%3A%2F%2Fheppee.com" target="_blank" title="Email" onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' +  encodeURIComponent(document.URL)); return false;"><img src="images/flat_web_icon_set/black/Email.png"></a></li>
                    </ul>
                </center>
            </div>
        </div>
    </div>



</div><!-- /.container -->

    <div class="container visible-sm" style="padding-top:12%;">

        <div class="col-md-6 col-lg-6 col-sm-6 hidden-xs">
            <a data-toggle="modal" data-target="#videoModal" style="cursor:pointer;">
                <img src="img/beta/iphones.png" alt="" class="phones img-responsive"/>
                <div style="position: absolute; background: url('http://dev.csda.net/wp-content/uploads/2013/11/play.png') center center no-repeat; width:100px; height:100px; top:125px;left:125px;"></div>
            </a>        </div>

        <div class="col-md-5">
            <img src="img/logo.png" alt="Heppee" class="logo">
            <div style="margin-top: 40px; margin-right: 25px;" class="pull-right">
                <a href="http://heppee.com/blog/pers-media/" style="color:#fff;">Pers</a> &nbsp;|&nbsp; <a href="http://heppee.com/blog/faq/" style="color:#fff;">FAQ</a>
            </div>
            <p class="lead">Let's make it a Childhood our kids will always remember</p>
            <p style="color:#818181;text-shadow: none;">De Co-ouderschap app voor ouders en kind. Meld je aan voor onze besloten betaversie!</p>

            <form id="subscribe" class="form" action="<?=$_SERVER['PHP_SELF']; ?>" method="post" style="margin-top: 25px; background:rgba(0,0,0,0.5); padding: 15px; padding-top:20px; border-radius:5px;">
                <span id="response"><? require_once('mailchimp2/inc/store-address.php'); if(isset($_GET['submit'])){ echo storeAddress(); } ?></span>
                <div class="form-group">
                    <label>Welk type smartphone heeft u?</label>
                    <br>
                    <input type="radio" name="phone" value="iphone" checked> &nbsp;iPhone
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" name="phone" value="android"> &nbsp;Android
                </div>
                <div class="form-group form-inline">
                    <div class="input-group">
                        <input size="25" type="email" class="form-control input-box required input-lg" id="email" name="email" placeholder="emailadres" style="border: 0px;">
                    <span class="input-group-btn">
                      <input type="submit" class="btn btn-primary standard-button2 ladda-button" style="padding-top: 12px; padding-bottom: 12px; padding-left: 25px; padding-right: 25px; background: #06ABFF; border:1px solid #06ABFF;" value="SUBSCRIBE">
                    </span>
                    </div>
                </div>
            </form>
        </div>

    </div><!-- /.container -->

</div>


<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/7RO65hqWb0s?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
            </div>
        </div>
    </div>
</div>

<style>
    .modal.fade .modal-dialog {
        -webkit-transition: -webkit-transform 0.3s ease-out !important;
        -moz-transition: -moz-transform 0.3s ease-out !important;
        -o-transition: -o-transform 0.3s ease-out !important;
        transition: transform 0.3s ease-out !important;
    }

    .modal.in .modal-dialog {

    }

    .share-buttons{
        list-style: none;
    }

    .share-buttons li{
        display: inline;
    }
</style>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

{{ HTML::script('mailchimp2/js/mailing-list.js') }}
</body>
</html>
