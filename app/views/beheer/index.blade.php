<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Aanmelden als testgezin voor de Heppee app!">
    <meta name="keywords" content="Heppee, aanmelden, test, gezin, testgezin, app, ios, iphone, android, kinderen, co-parenting, co-ouderschap, gescheiden, ouders">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE TITLE -->
    <title>Heppee - Aanmelden testgezin</title>

    {{ HTML::style('css/bootstrap.min.css') }}

    {{ HTML::style('assets/elegant-icons/style.css') }}
    {{ HTML::style('assets/app-icons/styles.css') }}

    <!--[if lte IE 7]>{{ HTML::style('lte-ie7.js') }}<![endif]-->

    {{ HTML::style('http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic') }}

    {{ HTML::style('css/owl.theme.css') }}
    {{ HTML::style('css/owl.carousel.css') }}
    {{ HTML::style('css/nivo-lightbox.css') }}
    {{ HTML::style('css/nivo_themes/default/default.css') }}

    {{ HTML::style('css/animate.min.css') }}
    {{ HTML::style('css/styles.css') }}
    {{ HTML::style('css/colors/blue.css') }}
    {{ HTML::style('css/responsive.css') }}

    <!--[if lt IE 9]>
    {{ HTML::script('js/html5shiv.js') }}
    {{ HTML::script('js/respond.min.js') }}
    <![endif]-->

    {{ HTML::style('http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css') }}

    {{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js') }}

</head>

<body>


<header class="header" data-stellar-background-ratio="0.5" id="home">

    <!-- COLOR OVER IMAGE -->
    <div class="color-overlay" style="height: 350px;"> <!-- To make header full screen. Use .full-screen class with color overlay. Example: <div class="color-overlay full-screen">  -->

        <!-- CONTAINER -->
        <div class="container">

            <!-- ONLY LOGO ON HEADER -->
            <div class="only-logo">
                <div class="navbar">
                    <div class="navbar-header">
                        <img src="{{ asset('img/logo.png') }}" alt="Heppee">
                    </div>
                </div>
            </div> <!-- /END ONLY LOGO ON HEADER -->


            <div class="row home-contents">
                <div class="col-md-12">
                        <h1 class="intro" style="color:#fff;text-align: left; margin-top: -25px;">Aanmeldingen</h1>
                </div>
            </div>
            <!-- /END ROW -->

        </div>
        <!-- /END CONTAINER -->
    </div>
    <!-- /END COLOR OVERLAY -->
</header>
<!-- /END HEADER -->
<div class="container">
    <br><br>
    <h3>Er zijn in totaal <strong>{{ $totalSignups }}</strong> aanmeldingen.</h3>
    <br>
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped" style="text-align: left;">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Naam</th>
                    <th>Rol</th>
                    <th>Email</th>
                    <th>Geb. jaar</th>
                    <th>Tel. nr.</th>
                    <th>Kinderen</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0; ?>
                @foreach($signups as $signup)

                <tr id="popover{{$i}}">
                    <td>{{ $signup->id }}</td>
                    <td>{{ $signup->name }}</td>
                    <td>{{ $signup->role }}</td>
                    <td>{{ $signup->email }}</td>
                    <td>{{ $signup->year }}</td>
                    <td>{{ $signup->phone }}</td>
                    <td>{{ $signup->countChildren() }}</td>
                </tr>

                <div id="popover{{$i}}Content" style="display: none">
                    <div>@foreach($signup->children as $child) {{ $child->gender }}, {{ $child->Year }} <br> @endforeach</div>
                </div>
                <?php $i++; ?>
                @endforeach
                </tbody>
            </table>

            <div id="popoverTitle" style="display: none">
                <b>Kinderen</b>
            </div>

        </div>
    </div>
</div>


{{ HTML::script('js/bootstrap.min.js') }}
{{ HTML::script('js/jquery.scrollTo.min.js') }}
{{ HTML::script('js/jquery.localScroll.min.js') }}
{{ HTML::script('js/owl.carousel.min.js') }}
{{ HTML::script('js/nivo-lightbox.min.js') }}
{{ HTML::script('js/simple-expand.min.js') }}
{{ HTML::script('js/wow.min.js') }}
{{ HTML::script('js/jquery.stellar.min.js') }}
{{ HTML::script('js/retina-1.1.0.min.js') }}
{{ HTML::script('js/jquery.nav.js') }}
{{ HTML::script('js/matchMedia.js') }}
{{ HTML::script('js/jquery.ajaxchimp.min.js') }}
{{ HTML::script('js/jquery.fitvids.js') }}
{{ HTML::script('js/custom.js') }}

<script>
    $(function(){

        @for($i=0;$i<$totalSignups;$i++)

            $("#popover{{$i}}").popover({
                html : true,
                container: 'body',
                content: function() {
                    return $('#popover{{$i}}Content').html();
                },
                title: function() {
                    return $('#popoverTitle').html();
                }
            });

        @endfor

    });
</script>

</body>
</html>