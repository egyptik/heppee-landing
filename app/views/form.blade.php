<h4>{{ Lang::get('form.your-details') }}</h4>

@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

{{ Form::open(array('route' => 'signups.store', 'class'=>'contact-form')) }}

<div class="col-md-6">
    {{ Form::text('name', '', ['class' => 'form-control input-box', 'placeholder' => Lang::get('form.name') ]) }}
</div>

<div class="col-md-6">
    {{ Form::text('email', '', ['class' => 'form-control input-box', 'placeholder' => Lang::get('form.email') ]) }}
</div>

<div class="col-md-4">
    {{ Form::text('phone', '', ['class' => 'form-control input-box', 'placeholder' => Lang::get('form.phone') ]) }}
</div>

<div class="col-md-4" style="padding-top:10px;">
    <div class="row">

        <div class="col-md-4" style="padding-top:11px;">
            <label for="exampleInputPassword1">{{ Lang::get('role') }}</label>
        </div>

        <div class="col-md-8">
            {{ Form::select('role', [
            'Moeder' => Lang::get('form.roles.mother'),
            'Vader' => Lang::get('form.roles.father'),
            'Stiefmoeder' => Lang::get('form.roles.stepmother'),
            'Stiefvader' => Lang::get('form.roles.stepfather')
            ], ['style' => 'width:100%']) }}
        </div>

    </div>

</div>


<div class="col-md-4" style="padding-top:10px;">
    <div class="row">

        <div class="col-md-7" style="padding-top:11px;">
            <label for="exampleInputPassword1">{{ Lang::get('form.birthyear') }}</label>
        </div>


        <div class="col-md-5">
            {{ Form::selectRange('year', 1945, 1998, ['style' => 'width:100%;']) }}
        </div>


    </div>
</div>


<div class="row">
    <div class="col-md-12" id="children">
        <hr>
        <h4>{{ Lang::get('form.children') }}</h4>

        <div class="col-md-6" style="padding-top:10px;">
            <div class="row">

                <div class="col-md-4" style="padding-top:11px;">
                    <label for="exampleInputPassword1">{{ Lang::get('form.genders.gender') }}</label>
                </div>

                <div class="col-md-8">
                    {{ Form::select('gender1', [
                    'Jongen' => Lang::get('form.genders.boy'),
                    'Meisje' => Lang::get('form.genders.girl')
                    ], ['style' => 'width:100%;']) }}
                </div>

            </div>

        </div>

        <div class="col-md-6" style="padding-top:10px;">
            <div class="row">

                <div class="col-md-6" style="padding-top:11px;">
                    <label for="exampleInputPassword1">{{ Lang::get('form.birthyear') }}</label>
                </div>

                <div class="col-md-6">
                    {{ Form::selectRange('year1', 1975, 2014 , ['style' => 'width:100%;']) }}
                </div>

            </div>

        </div>

    </div>
</div>
<br>
<button class="btn btn-link" id="addChild"><i class="icon_plus_alt2"></i> &nbsp;{{ Lang::get('form.add-child') }}</button>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<button class="btn btn-link disabled" id="remChild"><i class="icon_minus_alt2"></i> &nbsp;{{ Lang::get('form.delete-child') }}</button>

{{ Form::hidden('children', '1', ['id'=>'totalChildren']) }}

<br><br>

{{ Form::submit(Lang::get('form.submit'), ['class' => 'btn btn-primary standard-button2 ladda-button']) }}

{{ Form::close() }}