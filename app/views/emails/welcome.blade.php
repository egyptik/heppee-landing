Beste {{ $name }},<br>
<br>
Welkom in onze testgroep!<br>
<br>
Bedankt dat u ons wilt helpen bij het verkrijgen van informatie met betrekking tot de communicatie en organisatie tijdens een co-ouderschap situatie. Het gaat hierbij om zowel de communicatie tussen kind en ouders als tussen de ouders onderling.<br>
<br>
Wat kunt u van ons verwachten? De komende 2 maanden sturen we u wekelijks een update en/of een vraag.<br>
Graag neem ik binnenkort contact met u op om een afspraak voor een kort (tussen de 15 en 30 min) interview in te plannen. Dit kan telefonisch of live. Wat u natuurlijk het beste uitkomt!<br>
<br>
Uiteraard zullen wij erg zorgvuldig en vertrouwelijk met deze informatie omgaan en we willen u alvast bedanken voor uw medewerking.<br>
<br>
Heeft u vragen of wilt iets aan ons kwijt? Aarzel niet en neem contact op via sabrina@heppee.com<br>
<br>
<br>
Vriendelijke groet,<br>
<br>
Sabrina Wiarda-de Vries<br>
Coordinator Testgroep Co-Ouderschap