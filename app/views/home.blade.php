<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Aanmelden als testgezin voor de Heppee app!">
    <meta name="keywords" content="Heppee, aanmelden, test, gezin, testgezin, app, ios, iphone, android, kinderen, co-parenting, co-ouderschap, gescheiden, ouders">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE TITLE -->
    <title>Heppee</title>

    {{ HTML::style('css/bootstrap.min.css') }}

    {{ HTML::style('assets/elegant-icons/style.css') }}
    {{ HTML::style('assets/app-icons/styles.css') }}

    <!--[if lte IE 7]>{{ HTML::style('lte-ie7.js') }}<![endif]-->

    {{ HTML::style('http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic%7COxygen%3A400%2C300%2C700') }}

    {{ HTML::style('css/owl.theme.css') }}
    {{ HTML::style('css/owl.carousel.css') }}
    {{ HTML::style('css/nivo-lightbox.css') }}
    {{ HTML::style('css/nivo_themes/default/default.css') }}

    {{ HTML::style('css/animate.min.css') }}
    {{ HTML::style('css/styles.css') }}
    {{ HTML::style('css/colors/blue.css') }}
    {{ HTML::style('css/responsive.css') }}

    <!--[if lt IE 9]>
    {{ HTML::script('js/html5shiv.js') }}
    {{ HTML::script('js/respond.min.js') }}
    <![endif]-->

    {{ HTML::style('http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css') }}

    {{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js') }}

    <link rel='stylesheet' id='ivy_fonts-css'  href='' type='text/css' media='all' />

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-55465707-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>

<body>


<header class="header" data-stellar-background-ratio="0.5" id="home">

    <!-- COLOR OVER IMAGE -->
    <div class="color-overlay"> <!-- To make header full screen. Use .full-screen class with color overlay. Example: <div class="color-overlay full-screen">  -->

        <!-- CONTAINER -->
        <div class="container">

            <!-- ONLY LOGO ON HEADER -->
            <div class="only-logo">
                <div class="navbar">
                    <div class="navbar-header">
                        <img src="{{ asset('img/logo.png') }}" alt="Heppee">
                    </div>
                    <style type="text/css">

                        .nav-menu {
                            font-size: 14px;
                            text-transform: uppercase;
                            margin-left: 30px;
                        }

                        .nav {
                            margin-top: 70px !important;
                            text-align: center;
                        }

                        .navbar-nav {
                            margin: 0;
                        }

                        ul {
                            display: block;
                            list-style-type: disc;
                            -webkit-margin-before: 1em;
                            -webkit-margin-after: 1em;
                            -webkit-margin-start: 0px;
                            -webkit-margin-end: 0px;
                            -webkit-padding-start: 40px;
                        }

                        .nav li {
                            padding-right: 25px;
                            padding-left: 25px;
                            position: relative;
                            letter-spacing: 2px;
                            display: inline-block;
                            list-style: none;
                            font-family: Oxygen, Arial , Sans-serif;
                        }
                        .nav li:after {
                            width: 4px;
                            height: 4px;
                            background-color: #c09551;
                            content: "";
                            position: absolute;
                            right: 0;
                            top: 50%;
                            margin-top: -2px;
                            border-radius: 50%;
                            margin-right: -2px;
                        }

                        .nav li:last-of-type:after {
                            width: 0px;
                            height: 0px;
                            display:hidden;
                        }

                        .nav a {
                            padding-right: 0;
                            padding-left: 0;
                            border-bottom: 2px solid transparent;
                            padding-bottom: 11px;
                            font-weight: 700;
                            color: #fff;
                        }
                    </style>

                    <nav class="nav-menu"  style="display:inline-block;">
                        <ul class="nav navbar-nav">

                            <li><a href="http://www.heppee.com/">Home</a></li>
                            <li><a href="http://www.heppee.com/blog">Blog</a></li>
                            <li><a href="http://www.heppee.com/team">Team</a></li>
                            <li><a href="http://heppee.com/blog/pers-media/">Pers</a></li>

                        </ul>
                    </nav>
                </div>
            </div> <!-- /END ONLY LOGO ON HEADER -->


            <div class="row home-contents">
                <div class="col-md-6 col-sm-6">

                    <!-- HEADING AND BUTTONS -->
                    <div class="intro-section">

                        <!-- WELCOM MESSAGE -->
                        <h1 class="intro">{{ Lang::get('home.introTitle') }}</h1>
                        <h5>{{ Lang::get('home.intro') }}</h5>

                        <!-- BUTTON -->
                        <div class="buttons" id="download-button">

                            <a href="#about" class="btn btn-primary btn-lg standard-button2"><i class="fa fa-question-circle" style="vertical-align:baseline; margin-right:10px;"></i>{{ Lang::get('home.what') }}</a>

                            &nbsp;&nbsp;

                            <a href="#aanmelden" class="btn btn-default btn-lg standard-button"><i class="fa fa-child" style="vertical-align:baseline; margin-right:10px;"></i>{{ Lang::get('home.signup') }}</a>

                        </div>
                        <!-- /END BUTTONS -->

                    </div>
                    <!-- /END HEADNING AND BUTTONS -->

                </div>


                <div class="col-md-6 col-sm-6 hidden-xs">

                    <!-- PHONE IMAGE WILL BE HIDDEN IN TABLET PORTRAIT AND MOBILE-->
                    <div class="phone-image">
                        @if(Config::get('app.locale') == 'nl')
                            <img src="img/iphone.png" class="img-responsive" alt="">
                        @else
                            <img src="img/iphone-en.png" class="img-responsive" alt="">
                        @endif
                    </div>

                </div>

            </div>
            <!-- /END ROW -->

        </div>
        <!-- /END CONTAINER -->
    </div>
    <!-- /END COLOR OVERLAY -->
</header>
<!-- /END HEADER -->

<section class="app-brief grey-bg" id="brief1">

    <div class="container">

        <div class="row" id="about">

            <!-- PHONES IMAGE -->
            <div class="col-md-6 wow fadeInRight animated animated left-align" data-wow-offset="10" data-wow-duration="1.5s" style="visibility: visible;-webkit-animation-duration: 1.5s; -moz-animation-duration: 1.5s; animation-duration: 1.5s;">

                <!-- SECTION TITLE -->
                <h2 class="dark-text" style="text-transform:uppercase;">{{ Lang::get('home.what') }}</h2>

                <div class="colored-line-left">
                </div>

                <p>{{ Lang::get('home.about') }}</p>

            </div>

            <!-- RIGHT SIDE WITH BRIEF -->
            <div class="col-md-6 left-align wow fadeInLeft animated animated" data-wow-offset="10" data-wow-duration="1.5s" style="visibility: visible;-webkit-animation-duration: 1.5s; -moz-animation-duration: 1.5s; animation-duration:">

                <h3 style="margin-top:125px;">{{ Lang::get('home.aboutTitle') }}</h3>
                <p style="margin-top:0px;">{{ Lang::get('home.aboutDesc') }}</p>

            </div>
            <!-- /END RIGHT BRIEF -->

        </div>
        <!-- /END ROW -->

    </div>
    <!-- /END CONTAINER -->

</section>

<section class="features grey-bg" id="features" style="margin-top:-150px;">

    <div class="container">

        <div class="row">

            <div class="col-md-4 features-right"><!-- FEATURE -->
                <div class="feature">

                    <!-- ICON -->
                    <div class="icon-container">
                        <div class="icon">
                            <i class="icon_heart_alt"></i>
                        </div>
                    </div>

                    <!-- FEATURE HEADING AND DESCRIPTION -->
                    <div class="fetaure-details">
                        <h4 class="main-color">{{ Lang::get('home.emotionTitle') }}</h4>
                        <p style="font-size:12px;">
                            {{ Lang::get('home.emotionDesc') }}
                        </p>
                    </div>

                </div>
                <!-- /END SINGLE FEATURE --></div>

            <div class="col-md-4 features-right"><!-- FEATURE -->
                <div class="feature">

                    <!-- ICON -->
                    <div class="icon-container">
                        <div class="icon">
                            <i class="icon_calendar"></i>
                        </div>
                    </div>

                    <!-- FEATURE HEADING AND DESCRIPTION -->
                    <div class="fetaure-details">
                        <h4 class="main-color">{{ Lang::get('home.organisationTitle') }}</h4>
                        <p style="font-size:12px;">
                            {{ Lang::get('home.organisationDesc') }}
                        </p>
                    </div>

                </div>
                <!-- /END SINGLE FEATURE --></div>

            <div class="col-md-4 features-right"><!-- FEATURE -->
                <div class="feature">

                    <!-- ICON -->
                    <div class="icon-container">
                        <div class="icon">
                            <i class="icon_chat_alt"></i>
                        </div>
                    </div>

                    <!-- FEATURE HEADING AND DESCRIPTION -->
                    <div class="fetaure-details">
                        <h4 class="main-color">{{ Lang::get('home.communicationTitle') }}</h4>
                        <p style="font-size:12px;">
                            {{ Lang::get('home.communicationDesc') }}
                        </p>
                    </div>

                </div>
                <!-- /END SINGLE FEATURE --></div>

        </div>
        <!-- /END ROW -->

    </div>
    <!-- /END CONTAINER -->

</section>

<!-- =========================
     FOOTER
============================== -->
<footer>

<div class="container" id="formContainer">

<div class="contact-box wow" data-wow-offset="10" data-wow-duration="1.5s">

<!-- CONTACT BUTTON TO EXPAND OR COLLAPSE FORM -->

<a class="btn contact-button expand-form expanded"><i class="fa fa-child"></i></a>

<!-- EXPANDED CONTACT FORM -->
<div class="row expanded-contact-form" id="aanmelden">

    <div class="row">
        <br><br>
        <div class="col-md-4">
            <center>
                <a class="btn btn-default btn-lg standard-button" href="/testgezin" style="margin-top: -10px;margin-bottom: 50px;"><i class="fa fa-child" style="vertical-align:baseline; margin-right:10px;"></i>{{ Lang::get('home.openForm') }}</a>
            </center>
        </div>
        <div class="col-md-1">
            <center>
                OR
            </center>
        </div>

        <div class="col-md-7" style="margin-top: -35px;">
            <p>{{ Lang::get('home.appReady') }}</p><br>
            <form id="subscribe" class="form" action="<?=$_SERVER['PHP_SELF']; ?>" method="post" class="contact-form" style="margin-top: -18px;">
                <div class="form-group form-inline">
                    <input size="15" type="text" class="form-control input-box required" id="NewsletterName" name="name" placeholder="{{ Lang::get('form.name') }}" />
                    <input size="25" type="email" class="form-control input-box required" id="NewsletterEmail" name="email" placeholder="{{ Lang::get('form.email') }}" />
                    <input type="submit" class="btn btn-primary standard-button2 ladda-button" style="padding-top: 12px; padding-bottom: 12px;" value="{{ Lang::get('form.submit') }}" />
			  <span id="response">
				<? require_once('mailchimp/inc/store-address.php'); if(isset($_GET['submit'])){ echo storeAddress(); } ?>
			  </span>
                </div>
            </form>
        </div>

    </div>

</div>


</div>

</div>
    <section class="grey-bg">

        <div class="container">

            <div class="row" style="padding: 50px 0 125px 0;">

                <h2 class="dark-text">{{ Lang::get('home.partners') }}</h2>
                <br><br>

                <div class="col-md-2 col-md-offset-1">
                    <a href="http://innoleaps.com/" target="_blank"><img src="{{ asset('img/logos/innoleaps.png') }}" alt="InnoLeaps" width="100%"/></a>
                </div>

                <div class="col-md-2">
                    <a href="http://www.startupbootcamp.org/" target="_blank"><img src="{{ asset('img/logos/startupbootcamp.png') }}" alt="Startup Bootcamp" width="100%"/></a>
                </div>

                <div class="col-md-2">
                    <a href="http://social-enterprise.nl/" target="_blank"><img src="{{ asset('img/logos/social-enterprise.png') }}" alt="Social Enterprise NL" width="100%"/></a>
                </div>

                <div class="col-md-2">
                    <a href="http://www.vodafone.com/content/index/about/foundation.html" target="_blank"><img src="{{ asset('img/logos/vodafone.png') }}" alt="Vodafone" width="100%"/></a>
                </div>

                <div class="col-md-2">
                    <a href="http://waag.org/en" target="_blank"><img src="{{ asset('img/logos/waag.png') }}" alt="Waagsociety" width="100%"/></a>
                </div>

            </div>

            <div class="row" style="padding: 0px 0 125px 0;">

                <h2 class="dark-text">{{ Lang::get('home.get-in-touch') }}</h2>
                <p class="lead text-muted">{{ Lang::get('home.get-in-touch-desc') }}</p>

                <div class="col-md-4 col-md-offset-2" style="margin-top: -25px;">
                    <h2 class="center btn btn-large btn-contact">Thierry@heppee.com</h2>
                </div>

                <div class="col-md-4" style="margin-top: -25px;">
                    <h2 class="center btn btn-large btn-contact">+316 41 21 90 24</h2>
                </div>



            </div>

        </div>

    </section>


@include('footer')

</div>
</footer>
<!-- /END FOOTER -->

{{ HTML::script('js/bootstrap.min.js') }}
{{ HTML::script('js/jquery.scrollTo.min.js') }}
{{ HTML::script('js/jquery.localScroll.min.js') }}
{{ HTML::script('js/owl.carousel.min.js') }}
{{ HTML::script('js/nivo-lightbox.min.js') }}
{{ HTML::script('js/simple-expand.min.js') }}
{{ HTML::script('js/wow.min.js') }}
{{ HTML::script('js/jquery.stellar.min.js') }}
{{ HTML::script('js/retina-1.1.0.min.js') }}
{{ HTML::script('js/jquery.nav.js') }}
{{ HTML::script('js/matchMedia.js') }}
{{ HTML::script('js/jquery.ajaxchimp.min.js') }}
{{ HTML::script('js/jquery.fitvids.js') }}
{{ HTML::script('js/custom.js') }}

{{ HTML::script('mailchimp/js/mailing-list.js') }}

<script type="text/javascript">
    $( document ).ready(function() {

        $('#signupButton').on('click', function() {

            $('#signupButton').fadeOut(1000);
            $('#formContainer').fadeIn(1000);
            $("html, body").animate({ scrollTop: $('#aanmelden').offset().top }, 1);

        });

        @if($errors->any() || Session::has('success'))
        $("html, body").animate({ scrollTop: $('#aanmelden').offset().top }, 1);
        @endif

        // Adding new poll options

        var i = $('#totalChildren').val();

        $('#addChild').on('click', function() {
            i++;
            $('<div id="extraChild"><div class="col-md-6" style="padding-top:10px;"><div class="row"><div class="col-md-4" style="padding-top:11px;"><label for="exampleInputPassword1">{{ Lang::get('form.gender') }}</label></div><div class="col-md-8"><select name="gender'+i+'"><option value="Jongen">{{ Lang::get('form.roles.boy') }}</option><option value="Meisje">{{ Lang::get('form.roles.girl') }}</option></select></div></div></div><div class="col-md-6" style="padding-top:10px;"><div class="row"><div class="col-md-6" style="padding-top:11px;"><label for="exampleInputPassword1">{{ Lang::get('form.birthyear') }}</label></div><div class="col-md-6"><select name="year'+i+'"><option value="1975">1975</option><option value="1976">1976</option><option value="1977">1977</option><option value="1978">1978</option><option value="1979">1979</option><option value="1980">1980</option><option value="1981">1981</option><option value="1982">1982</option><option value="1983">1983</option><option value="1984">1984</option><option value="1985">1985</option><option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option></select></div></div></div></div>').appendTo('#children');

            if(i > 1) {
                $('#remChild').removeClass('disabled');
            }

            $('#totalChildren').attr('value', i);

            return false;
        });

        $('#remChild').on('click', function() {
            if( i > 1 ) {
                $('#extraChild:last-of-type').remove();
                i--;
                if ( i == 1 ) { $('#remChild').addClass('disabled'); }
            }

            $('#totalChildren').attr('value', i);

            return false;
        });

    });
</script>


</body>
</html>