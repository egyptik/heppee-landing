<?php

//IP

Route::group(array('before' => 'lang'), function()
{

    Route::get('/', function()
    {

        //return View::make('home');
        return View::make('beta');

    });

    Route::get('/testgezin', function()
    {
        return View::make('testgezin');
    });

    Route::post('/', ['as' => 'signups.store', 'uses' => 'SignupController@store']);

    Route::get('/beheer', [
        'before' => 'auth.basic',
        'uses' => 'SignupController@index'
    ]);

    Route::get('/team', [
        'uses' => 'TeamController@index'
    ]);

});