<?php

class TeamController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /team
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('team');
	}

}