<?php

class SignupController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 * POST /signup
	 *
	 * @return Response
	 */
    public function store()
    {

        $rules = array(
            'name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|numeric|min:7',
            'year'  => 'required',
            'role'  => 'required'
        );

        for($i=1;$i<=Input::get('children');$i++) {
            $rules['gender'.$i] = 'required';
            $rules['year'.$i] = 'required';
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }

        $signup = new Signup();
        $signup->name = Input::get('name');
        $signup->phone = Input::get('phone');
        $signup->email = Input::get('email');
        $signup->role = Input::get('role');
        $signup->year = Input::get('year');
        $signup->save();

        for($i=1;$i<=Input::get('children');$i++) {
            $child = new Child();
            $child->gender = Input::get('gender'.$i);
            $child->year = Input::get('year'.$i);
            $child->signup_id = $signup->id;
            $child->save();
        }

        Mail::send('emails.welcome', ['name' => Input::get('name')], function($message)
        {
            $message->to(Input::get('email'), Input::get('name'))->subject('Welkom bij de Testgroep Co-Ouderschap');
        });

        return Redirect::to('/testgezin')->with('success', 'Bedankt voor uw aanmelding!');

    }

    public function index()
    {

        $signups = Signup::all();
        return View::make('beheer.index')->with(['signups' => $signups, 'totalSignups' => $signups->count()]);

    }

}