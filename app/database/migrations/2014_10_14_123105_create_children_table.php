<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChildrenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('children', function(Blueprint $table)
		{
			$table->increments('id');
            $table->enum('gender', ['Jongen', 'Meisje']);
            $table->smallInteger('Year')->unsigned();
            $table->integer('signup_id')->unsigned();
			$table->timestamps();

            $table->foreign('signup_id')
                ->references('id')->on('signups')
                ->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('children');
	}

}
