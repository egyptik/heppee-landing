<?php

class Child extends \Eloquent {
	protected $fillable = [];

    protected $table = 'children';

    public function signup()
    {
        return $this->belongsTo('Signup');
    }
}