<?php

class Signup extends \Eloquent {
	protected $fillable = [];

    protected $table = 'signups';

    public function children()
    {
        return $this->hasMany('Child');
    }

    public function countChildren()
    {
        return $this->children->count();
    }

}